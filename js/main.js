const LIVE_DATA = [
    [
        '今天19:00开始',
        'https://edu-image.nosdn.127.net/80ac6898cb9a42af90e69e6923bce945.jpg?imageView&quality=100&thumbnail=180y180',
        '23考研数学基础入门与进阶丨刘金峰',
        '刘金峰老师'
    ],
    [
        '今天16:00开始',
        'https://edu-image.nosdn.127.net/86175286e3a44738bfaf7b9ad52029ec.png?imageView&quality=100&thumbnail=164y188',
        '专升本英语备考规划（11月14日）',
        '陈正康老师'
    ],
    [
        '今天19:30开始',
        'https://edu-image.nosdn.127.net/853d76292e6d49db8bb93fa7ed8eb3c1.jpg?imageView&quality=100',
        '专升本英语作文写作技巧',
        'Irene老师'
    ],
    [
        '今天19:30开始',
        'https://edu-image.nosdn.127.net/eb876c35ae3e4f21bf742e02a21ead26.png?imageView&quality=100&thumbnail=461y435',
        '看漫画速拓6000词',
        '祁连山老师'
    ],
    [
        '11月15日 21:00开始',
        'https://edu-image.nosdn.127.net/1dca4515b11b4248807a0c7a4a9d442d.png?imageView&quality=100&thumbnail=437y422',
        '四六级备考—听力训练',
        '李卓然老师'
    ],
    [
        '11月15日 19:30开始',
        'https://edu-image.nosdn.127.net/86175286e3a44738bfaf7b9ad52029ec.png?imageView&quality=100&thumbnail=164y188',
        '四六级高频核心词高效记忆法（11月15日）',
        '陈正康老师'
    ],
    [
        '11月15日 19:00开始',
        'https://edu-image.nosdn.127.net/80ac6898cb9a42af90e69e6923bce945.jpg?imageView&quality=100&thumbnail=180y180',
        '专升本数学备考及核心考点',
        '刘金峰老师'
    ],
    [
        '11月16日 19:00开始',
        'https://edu-image.nosdn.127.net/c8e8e04009bc45368b9bbab3b8724e8e.jpg?imageView&quality=100',
        '教资面试｜教资面试备考建议—点亮你的第一面！',
        '梦圆老师'
    ],
];
const RECOMMEND_DATA = [
    [
        'https://edu-image.nosdn.127.net/96e2df70728d4ff0a3e3b84c844c5bc5.jpg?imageView&quality=100&thumbnail=230y130&type=webp',
        '数据库原理——GaussDB云数据库',
        '东北师范大学',
        '405人参加',
        '运用数据库技术解决实际应用'
    ],
    [
        'https://edu-image.nosdn.127.net/6b07c197224d462296b7738c37d11f93.jpg?imageView&quality=100&thumbnail=230y130&type=webp',
        '船舶动力装置原理与设计',
        '华中科技大学',
        '95人参加',
        '它为什么被誉为”船舶心脏“？'
    ],
    [
        'https://edu-image.nosdn.127.net/5646602a17bf4567ae61d48f0c5dec75.png?imageView&quality=100&thumbnail=230y130&type=webp',
        '射频放大器设计——理论与实践',
        '杭州电子科技大学',
        '101人参加',
        '从理论和实践为创新设计做基础'
    ],
    [
        'https://edu-image.nosdn.127.net/f93ff33c60ba403ebaf59b6b77430805.jpg?imageView&quality=100&thumbnail=230y130&type=webp',
        '电路与电子学',
        '杭州电子科技大学',
        '151人参加',
        '了解我国电路电子事业发展的概况'
    ],
    [
        'https://edu-image.nosdn.127.net/D2AFD432D6563BB0888BA12B26B8D8BE.png?imageView&quality=100&thumbnail=230y130&type=webp',
        '创新思维与方法',
        '武汉大学',
        '1575人参加',
        '成为梦想与实干兼具的复合型人才'
    ]
];
const RANK_DATA = [
    {
        'title': '热门排行',
        'top': true,
        'summary': 'POPULAR COURSES',
        'data': [
            [
                'https://edu-image.nosdn.127.net/5B8826377EE623C7B6328E8F8B8D2871.png?imageView&quality=100&thumbnail=230y130',
                'Python语言程序设计',
                '北京理工大学',
                '163399人参加'
            ],
            [
                'https://edu-image.nosdn.127.net/E168A15535FA40E9A6D24C044469C9C5.jpg?imageView&quality=100&thumbnail=230y130',
                '心理学与生活',
                '南京大学',
                '199251人参加'
            ],
            [
                'https://img-ph-mirror.nosdn.127.net/tYhzuDVilzlDOo2bEyH_Qg==/6608226511143817333.jpg?imageView&quality=100&thumbnail=230y130',
                '程序设计入门——C语言',
                '浙江大学',
                '183993人参加'
            ],
            [
                'https://edu-image.nosdn.127.net/579ff23d10b44e939e8628d51cb77367.png?imageView&quality=100&thumbnail=230y130',
                '猴博士大会员',
                '',
                '25930人参加'
            ],
            [
                'https://img-ph-mirror.nosdn.127.net/S_KZHbKd1cVnNakzi7s3nw==/39125021863241218.png?imageView&quality=100&thumbnail=230y130',
                '高等数学（二）',
                '同济大学',
                '41209人参加'
            ]
        ]
    },
    {
        'title': '新课排行',
        'top': false,
        'summary': 'NEW COURSES',
        'data': [
            [
                'https://edu-image.nosdn.127.net/CE438AB8C6974A2C5BD6F4306A644920.jpg?imageView&quality=100&thumbnail=230y130',
                '内科学（呼吸系统疾病）',
                '西安交通大学',
                '2744人参加'
            ],
            [
                'https://edu-image.nosdn.127.net/72FCF74D18CECF0F20965A2EC1925DC2.jpg?imageView&quality=100&thumbnail=230y130',
                '思想道德修养与法律基础',
                '长安大学',
                '2245人参加'
            ],
            [
                'https://edu-image.nosdn.127.net/f3670d362b3a4b98b6d8bd5971fd420a.png?imageView&quality=100&thumbnail=230y130',
                '22考研政英数8小时押题预测',
                '',
                '1477人参加'
            ],
            [
                'https://edu-image.nosdn.127.net/bb911ff109564d21897c8b95ad432117.jpg?imageView&quality=100&thumbnail=230y130',
                '四级考前紧急救援9小时',
                '',
                '1461人参加'
            ],
            [
                'https://edu-image.nosdn.127.net/A3679A12D621797B2F48C70F8300BA4B.jpg?imageView&quality=100&thumbnail=230y130',
                '环境设施设计',
                '江南大学',
                '1304人参加'
            ]
        ]
    },
    {
        'title': '五星评价',
        'top': true,
        'summary': 'FIVE-STAR',
        'data': [
            [
                'https://edu-image.nosdn.127.net/834ac2486a2d4984b9be33c2eec2e390.png?imageView&quality=100&thumbnail=230y130',
                '知识论寻绎',
                '中国政法大学',
                '1408人参加'
            ],
            [
                'https://edu-image.nosdn.127.net/579ff23d10b44e939e8628d51cb77367.png?imageView&quality=100&thumbnail=230y130',
                '猴博士大会员',
                '',
                '25930人参加'
            ],
            [
                'https://edu-image.nosdn.127.net/8C8E45F668F4B4947F30062C20AD09DD.jpg?imageView&quality=100&thumbnail=230y130',
                '组织学与胚胎学',
                '广西医科大学',
                '4363人参加'
            ],
            [
                'https://edu-image.nosdn.127.net/e3ec68516f3645dab5b32d439d47a51a.jpg?imageView&quality=100&thumbnail=230y130',
                '婚恋-职场-人格',
                '武汉理工大学',
                '41421人参加'
            ],
            [
                'https://edu-image.nosdn.127.net/9b5579c010ce47b7b33a25665863ac85.png?imageView&quality=100&thumbnail=230y130',
                '新时代高校劳动教育通论',
                '山东管理学院',
                '2608人参加'
            ]
        ]
    }
];
const COMMENT_DATA = [
    [
        'https://edu-image.nosdn.127.net/74350F76DB090DAB803023429E67F45A.jpg?imageView&amp;thumbnail=310y308&amp;quality=100',
        '挖矿静女',
        '这里有取之不尽用之不竭的知识宝藏，我愿做一块海绵，每天来这儿吐故纳新，与时俱进。感谢每位老师的无私分享。',
        '来自《内科学（呼吸系统疾病）》'
    ],
    [
        'https://mc.stu.126.net/res/images/ui/profile-image.png',
        '一十一232312121',
        '强烈推荐！！！！第一节课首先就收获了很多地道的词汇，其次对于想去外资企业的人 帮助很大，思维、架构！！！',
        '来自《商务英语》'
    ],
    [
        'https://img-ph-mirror.nosdn.127.net/sJARt-uTgAMpSpSlgsYjpA==/6597734971192760059.png',
        'zhangzl419',
        '我爱C语言，我喜欢翁恺老师的课，我学这门课不是为了学习知识，一种对C语言和我喜欢的老师的情怀。请注意，我不是在开玩笑，而是真话。C语言是我接触的第二门计算机语言，也是向我打开计算机世界的一门语言，它小巧、简介、表达力丰富，有很多的软件都是以C语言写作完成的。翁恺老师是我在2010年接触到的，当时，我们学校的图书馆有一个视频教程，就是翁老师i讲解的，讲的是web系统制作的技术，我仔细听过这门视频课程，发现翁老师对教育、对计算机充满了热情，我能感受到老师对计算机的热爱，我被他深深的感染了，之后我陆续从网上跟着老师的视频教程学习了C  、Java等课程。看到这门《C语言程序设计进阶》的时候，我也就顺利成章的学习了。感谢翁老师，虽然没有谋面，但是他的真诚、热情已经深深的影响了一个北京的学生的学业和事业。目前我在中国科学院工作，是一名工程师，我热爱我的工作，这种热爱是受到翁老师的感染而加深了的。',
        '来自《C语言程序设计进阶》'
    ],
    [
        'https://edu-image.nosdn.127.net/E1FA7146D5F783BBCEC29338D51C72DF.jpg?imageView&amp;thumbnail=155x102&amp;quality=100',
        'mooc331699',
        '这个课不能再好了！！我太感谢杨老师了！！条理清晰，逻辑明了，这些枯燥的理论都讲明白了，而且语言都是经过雕琢，语言很动听很高级。我是今年的研究生考生，复试准备期间看了课程三遍，很有收获，希望赶紧上岸‎|•\'-\'•)و✧',
        '来自《翻译导论》'
    ]
];

const live_list = $('#live');
const recommend_list = $('#recommend');
const rank_list = $('#rank');
const comment_list = $('#comment .list');
const login = $('#login');

const banner = $('#banner');

let bannerImages = [
    'https://edu-image.nosdn.127.net/77485a0370d24edb8edc2e024460684c.jpg?imageView&quality=100',
    'http://edu-image.nosdn.127.net/7f5bb714d9614248a2809987eb38d45f.png?imageView&quality=100',
    'http://edu-image.nosdn.127.net/83337699a9f84954b429752430e12d29.png?imageView&quality=100',
    'http://edu-image.nosdn.127.net/01ff2338ae3b4e8a847485f0c114ad18.png?imageView&quality=100',
    'http://edu-image.nosdn.127.net/d6531e72b38c46ca9373fec1b08ad72b.png?imageView&quality=100&thumbnail=776y360',
    'http://edu-image.nosdn.127.net/8539ab83cfe14443aae0067f10d306b2.png?imageView&quality=100'
];
let bannerIndex = 0;

$('.panel a:nth-child(1) img').mouseover(function () {
    $('body .panel a:nth-child(1) img').attr('src', 'img/feedback-hover.png');
});
$('.panel a:nth-child(1) img').mouseleave(function () {
    $('body .panel a:nth-child(1) img').attr('src', 'img/feedback.png');
});
$('.panel a:nth-child(3) img').mouseover(function () {
    $('body .panel a:nth-child(3) img').attr('src', 'img/totop-hover.png');
});
$('.panel a:nth-child(3) img').mouseleave(function () {
    $('body .panel a:nth-child(3) img').attr('src', 'img/totop.png');
});
$('.third .head input').focus(function () {
    $('.third .head').attr('style', 'border: 1px solid #00CC7E');
});
$('.third .password input').focus(function () {
    $('.third .password').attr('style', 'border: 1px solid #00CC7E');
});
$('.third .head input').blur(function () {
    $('.third .head').attr('style', 'border: 1px solid #ddd');
});
$('.third .password input').blur(function () {
    $('.third .password').attr('style', 'border: 1px solid #ddd');
});

$(window).scroll(function (event) {
    let height = $(window).scrollTop();
    if (height === 0) {
        $('.panel a:nth-child(3)').hide();
    } else {
        $('.panel a:nth-child(3)').show();
    }
});

setInterval(changeBanner, 3000);

function showData() {
    LIVE_DATA.forEach(function (item) {
        live_list.append(getLiveItem(item[0], item[1], item[2], item[3]));
    });
    RECOMMEND_DATA.forEach(function (item) {
        recommend_list.append(getRecommendItem(item[0], item[1], item[2], item[3], item[4]));
    });
    RANK_DATA.forEach(function (item) {
        let title = item.title;
        let top = item.top;
        let summary = item.summary;
        let list = item.data;
        let listHtml = '';
        let rank = 1;
        list.forEach(function (sub) {
            listHtml += getRankListItem(rank, sub[0], sub[1], sub[2], sub[3]);
            rank++;
        });
        rank_list.append(getRankItem(title, top, summary, listHtml));
    });
    COMMENT_DATA.forEach(function (item) {
        comment_list.append(getCommentItem(item[0], item[1], item[2], item[3]));
    });
}

function getLiveItem(time, img, title, teacher) {
    return `
        <a href="html/live.html" target="_blank">
            <div class="live-item rise-shadow">
                <div class="time">` + time + `</div>
                <div class="live-content">
                    <img src="` + img + `" alt="">
                    <div class="live-info">
                        <div class="title">` + title + `</div>
                        <div class="teacher">` + teacher + `</div>
                    </div>
                </div>
            </div>
        </a>
    `;
}

function getRecommendItem(img, title, school, joined, tip) {
    return `
        <a href="html/math.html" target="_blank">
            <div class="recommend-item rise-shadow">
                <img src="` + img + `" alt="">
                <div class="recommend-content">
                    <h3>` + title + `</h3>
                    <p>` + school + `</p>
                    <div class="joined">` + joined + `</div>
                    <div class="top-tip">` + tip + `</div>
                </div>
            </div>
        </a>
    `;
}

function getRankItem(title, top, summary, list) {
    return `
        <div class="rank-content">
            <div class="header">
                <img src="https://edu-image.nosdn.127.net/3e29dfc47a3f4ff6836453607094406d.jpg?imageView&quality=100" alt="">
                <p class="title">` + title + `</p>
                <p class="top-tip">` + (top ? 'TOP50' : '') + `</p>
                <p class="right-tip">` + summary + `</p>
            </div>
            <div class="content">` + list + `</div>
        </div>
    `;
}

function getRankListItem(rank, img, title, school, joined) {
    return `
        <div class="item">
            <p>` + rank + `</p>
            <img src="` + img + `" alt="">
            <div class="item-content-info">
                <div class="title">` + title + `</div>
                <div class="school">` + school + `</div>
                <div class="joined">` + joined + `</div>
            </div>
        </div>
    `;
}

function getCommentItem(img, title, content, from) {
    return `
        <a class="round-background rise-shadow" href="#">
            <img src="` + img + `" alt="">
            <p class="title">` + title + `</p>
            <p class="content">` + content + `</p>
            <div class="from">` + from + `</div>
        </a>
    `;
}

function scrollToTop() {
    $(window).scrollTop(0);
}

function showLogin() {
    login.show();
}

function closeLogin() {
    login.hide();
}

function changeBanner() {
    bannerIndex++;
    bannerIndex = bannerIndex % 6;
    banner.attr('src', bannerImages[bannerIndex]);
    $(banner).fadeOut(100);
    $(banner).fadeIn(200);
    $('.position > .eclipse').removeClass('eclipse-active');
    $('.position > .eclipse:nth-child(' + (bannerIndex + 1) + ')').addClass('eclipse-active');
}

showData();