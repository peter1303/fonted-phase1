const TOP = $('.top-background');

$(window).scroll(function (event) {
    let height = $(window).scrollTop();
    if (height === 0) {
        TOP.attr('style', 'background-color: #f5f5f5');
    } else {
        TOP.attr('style', 'background-color: #fff');
    }
});