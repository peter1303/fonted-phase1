# Fonted-Phase1

## 用法

### 圆形背景

```
.round-background
```

![输入图片说明](https://images.gitee.com/uploads/images/2021/1112/193510_0956ef2a_525583.png "屏幕截图.png")

## 阴影

> 鼠标移入有效

```
.rise-shadow
```
![输入图片说明](https://images.gitee.com/uploads/images/2021/1112/195330_3ee84e9f_525583.png "屏幕截图.png")

### 边距

```
.margin-top
```

```
.margin-right
```

```
.margin-bottom
```

```
.margin-left
```

## 椭圆
### 实心
```
.eclipse
```
![输入图片说明](https://images.gitee.com/uploads/images/2021/1113/151706_17e25f80_525583.png "屏幕截图.png")

## 初始化

> 首先在`head`引入`Jqurey`

```
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
```

> 然后在`body`下面引入`init.js`
```
<script src="js/init.js"></script>
```

![输入图片说明](https://images.gitee.com/uploads/images/2021/1113/140931_044f5121_525583.png "屏幕截图.png")